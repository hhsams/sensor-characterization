import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits

#path = r"F:/TheiaStuff/Sensori_mootmised/SensorThree/Raw/Bias/1.0/"
path = r"F:/TheiaStuff/Sensori_mootmised/SensorTest/Zbias/"
image = np.fromfile(path+'2_38_0_HG.raw', dtype='>u2') #Big endian uint16
image.shape = (2048, 2048)
print(image)


#displaying image
image = image.astype(np.float)
#image = image[750:1250][750:1250]
print(np.mean(image), np.std(image), np.median(image))
hdu=fits.PrimaryHDU(image)
hdul = fits.HDUList([hdu])
hdul.writeto('testh1.fits')


image = np.fromfile(path+'2_38_0_LG.raw', dtype='>u2') #Big endian uint16
image.shape = (2048, 2048)
#print(image)


#displaying image
image = image.astype(np.float)
#image = image[750:1250][750:1250]
print(np.mean(image[750:1250][750:1250]), np.std(image), np.median(image))
hdu=fits.PrimaryHDU(image)
hdul = fits.HDUList([hdu])
hdul.writeto('testl1.fits')


