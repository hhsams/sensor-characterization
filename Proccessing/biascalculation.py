import numpy as np
import matplotlib.pyplot as plt
import glob
import time

#image = np.fromfile('38_temp32.1348_gain50_it46_0_HG.raw', dtype='>u2') #Big endian uint16
# image.shape = (2048, 2048)



#image = image.astype(np.float)


#Where the images are
path = r"F:/TheiaStuff/Sensori_mootmised/SensorFour/"

start = time.time()

temperature_list = [16.0,21.0,26.0,31.0,36.0,41.0]#, 16.000,21.000,
                    #26.000,31.000,36.000,41.000]

#Read all the LG images
for temp in temperature_list:
    lg_array = []
    for name in glob.glob(path+'Raw/Bias/' + str(temp) + '/0_*_LG.raw'):
        print(name)
        image = np.fromfile(name, dtype='>u2')
        image.shape = (2048, 2048)
        lg_array.append(image)
    lg_array = np.array(lg_array)
    #Add, take mean, round and convert back to uint16
    lg_array_mean = np.round(np.sum(lg_array, 0)/len(lg_array))
    lg_array_mean = lg_array_mean.astype(np.uint16)
    np.save(path + 'Bias/' +str(temp)+ '_Bias_LG' + '.npy', lg_array_mean)
#print(np.mean(lg_array_mean))


#plt.imshow(lg_array_mean, cmap='gray', vmin=0, vmax = 4096)
#plt.show()

#Read all the HG images
for temp in temperature_list:
    hg_array = []
    for name in glob.glob(path +'Raw/Bias/' + str(temp) + '/0_*_HG.raw'):
        print(name)
        image = np.fromfile(name, dtype='>u2')
        image.shape = (2048, 2048)
        hg_array.append(image)
    hg_array = np.array(hg_array)
    #Add, take mean, round and convert back to uint16
    hg_array_mean = np.round(np.sum(hg_array, 0)/len(hg_array))
    hg_array_mean = hg_array_mean.astype(np.uint16)
    np.save(path + 'Bias/' +str(temp)+ '_Bias_HG' + '.npy', hg_array_mean)

#plt.imshow(hg_array_mean, cmap='gray', vmin=0, vmax = 4096)
#plt.show()

end = time.time()
#print(end - start)

##    hdu=fits.PrimaryHDU(lg_array_mean)
##    hdul = fits.HDUList([hdu])
##    hdul.writeto('analyze_testbias.fits')
    




