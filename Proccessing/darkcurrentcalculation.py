import numpy as np
import matplotlib.pyplot as plt
import glob
import time
from scipy import stats

    
# Where the images are
path = r"F:/TheiaStuff/Sensori_mootmised/SensorFour/"

start = time.time()

temperature_list = [16.0,21.0,26.0,31.0,36.0,41.0]
#exposure_list = [400,600,800,1000,1200,1400]
exposure_list = [35714,53571,71428,89285,107142,125000]

# Load in Bias's
lg_bias = []
hg_bias = []
for temp in temperature_list:
    lg_bias.append(np.load(path + 'Bias/' + str(temp) + "_Bias_LG.npy"))
    hg_bias.append(np.load(path + 'Bias/' + str(temp) + "_Bias_HG.npy"))

lg_array = []
hg_array = []
n = 0
#LG images
for temp in temperature_list:
    for exp in exposure_list:
        for name in glob.glob(path + 'Raw/DarkCurrent/'+str(temp) + '/' + str(exp) + '_*_LG.raw'):
            print(name)
            image = np.fromfile(name, dtype='>i2')
            image.shape = (2048, 2048)
            image = image-lg_bias[n]   #Substract bias
            image[image<0] = 0      #Incase bias is higher then signal
            lg_array.append(image)
        lg_array = np.array(lg_array)
        #Add, take mean, round and convert back to uint16
        lg_array_mean = np.round(np.sum(lg_array, 0)/len(lg_array))
        lg_array_mean = lg_array_mean.astype(np.int16)
        np.save(path + 'DC/LG_' +str(temp)+'_'+str(exp)+ '.npy', lg_array_mean)
        lg_array = []
    n +=1
n = 0     
for temp in temperature_list:
    for exp in exposure_list:
        for name in glob.glob(path + 'Raw/DarkCurrent/'+str(temp) + '/' + str(exp) + '_*_HG.raw'):
            print(name)
            image = np.fromfile(name, dtype='>i2')
            image.shape = (2048, 2048)
            image = image-hg_bias[n]   #Substract bias
            image[image<0] = 0      #Incase bias is higher then signal
            hg_array.append(image)
        hg_array = np.array(hg_array)
        #Add, take mean, round and convert back to uint16
        hg_array_mean = np.round(np.sum(hg_array, 0)/len(hg_array))
        hg_array_mean = hg_array_mean.astype(np.int16)
        np.save(path + 'DC/HG_' +str(temp)+'_'+str(exp)+ '.npy', hg_array_mean)
        hg_array = []
    n +=1

end = time.time()
print("Code took")
print(round(end - start, 3))
print("seconds")
"""




