import numpy as np
import matplotlib.pyplot as plt
import glob
import time
from scipy import stats

    
# Where the images are
path = r"F:/TheiaStuff/Sensori_mootmised/"

start = time.time()

temperature_list = [16.0,21.0,26.0,31.0,36.0,41.0]

exposure_list = [35714,53571,71428,89285,107142,125000]
sensor_name = ["SensorOne","SensorTwo","SensorThree","SensorFour"]

LG_darkLevels = []
for sensor in sensor_name:
	sensor_list = []
	for temp in temperature_list:
		a = [] # Create dummy array
		for exp in exposure_list:
			for name in glob.glob(path + sensor + '/DC/LG_'+str(temp) + '_' + str(exp) + '.npy'):
				image = np.load(name)
				a.append(np.mean(image))
		sensor_list.append(a)
		a = []
	LG_darkLevels.append(sensor_list)
	
HG_darkLevels = []
for sensor in sensor_name:
	sensor_list = []
	for temp in temperature_list:
		a = [] # Create dummy array
		for exp in exposure_list:
			for name in glob.glob(path + sensor + '/DC/HG_'+str(temp) + '_' + str(exp) + '.npy'):
				image = np.load(name)
				a.append(np.mean(image))
				#a.append(image[500][500])
		sensor_list.append(a)
		a = []
	HG_darkLevels.append(sensor_list)

print(len(LG_darkLevels[0]),len(HG_darkLevels[0]))
#temperature_list = [5,10,15,20,25,30,35,40,45,50]
# Calculate linear regression from the data
exposure_list = np.array([400,600,800,1000,1200,1400])/1000
#HGs, HGintercept, r_value, p_value, std_err = stats.linregress(exposure_list[1:],HG_darkLevels[0][1:])
#LGs, LGintercept, r_value, p_value, std_err = stats.linregress(exposure_list[1:],LG_darkLevels[0][1:])
#fit_HG = np.asarray(exposure_list)*HGs+HGintercept
#fit_LG = np.asarray(exposure_list)*LGs+LGintercept
# Plot the stuff
x = exposure_list

fig1 = plt.figure(figsize=(34,16))
ax1 = fig1.add_subplot(2,2,1)
ax2 = fig1.add_subplot(2,2,3)

colors = ['b','g','r','c','m','y','k','b','g','r']
temp_labels = ["16 C","21 C","26 C","31 C","36 C","41 C"]
"""
# HG plot
for i in range(len(sensor_name)):
    for j in range(len(temperature_list)):
        print(i,j)
        ax1.plot(x,HG_darkLevels[i][j],colors[j],label = temp_labels[2])

# LG plot
for i in range(len(sensor_name)):
    for j in range(len(temperature_list)):
        ax2.plot(x,LG_darkLevels[i][2],colors[j],label = temp_labels[2])
"""

ax1.plot(x,HG_darkLevels[0][2], "b",  label = "Sensor 1")
ax1.plot(x,HG_darkLevels[1][2], "y", label = "Sensor 2")
ax1.plot(x,HG_darkLevels[2][2], "g", label = "Sensor 3")
ax1.plot(x,HG_darkLevels[3][2], "r", label = "Sensor 4")



ax2.plot(x,LG_darkLevels[0][2], "b", label = "Sensor 1")
ax2.plot(x,LG_darkLevels[1][2], "y", label = "Sensor 2")
ax2.plot(x,LG_darkLevels[2][2], "g", label = "Sensor 3")
ax2.plot(x,LG_darkLevels[3][2], "r", label = "Sensor 4")


ax1.set_xlabel("Exposure (s)",fontsize=12)
ax1.set_ylabel("ADU",fontsize=12,labelpad=10)
ax1.set_title("HG Dark Current against exposure time",fontsize=16)
ax1.grid(True,which='both')
ax1.legend(loc='upper left')
#ax1.text(0.7,50,"slope ="+ str(round(HGs,3)))
#ax2.text(0.7,8,"slope ="+ str(round(LGs,3)))
ax2.set_xlabel("Exposure (s)",fontsize=12)
ax2.set_ylabel("ADU",fontsize=12,labelpad=10)
ax2.set_title("LG Dark Current against exposure time",fontsize=16)
ax2.grid(True,which='both')
ax2.legend(loc='upper left')

plt.show()
# Graphs on drak current change against temperature            

x = temperature_list
x1 = [20.5, 25.5, 29.5, 34.3, 39.3, 44.1]
x2 = [21.5, 26.6, 30.1, 35.4, 39.6, 45.4]
x3 = [20.9,25.7,30.2,35.0,39.9,44.9]
x4 = [21.4,26.0,30.8,36.1,41.1,46.2]
exposure_list = np.array([400,600,800,1000,1200,1400])/1000


HG_T = []
for i in range(len(sensor_name)):
    t = []
    for j in range(len(exposure_list)):
        a = []
        for k in range(len(temperature_list)):
            a.append(HG_darkLevels[i][k][j])
        t.append(a)
    HG_T.append(t)
LG_T = []
for i in range(len(sensor_name)):
    t = []
    for j in range(len(exposure_list)):
        a = []
        for k in range(len(temperature_list)):
            a.append(LG_darkLevels[i][k][j])
        t.append(a)
    LG_T.append(t)

exp = 3
HG_T_1s, HG_T_1_i, r_value, p_value, std_err = stats.linregress(x1,np.log10(HG_T[0][exp]))
HG_T_2s, HG_T_2_i, r_value, p_value, std_err = stats.linregress(x1,np.log10(HG_T[1][exp]))
HG_T_3s, HG_T_3_i, r_value, p_value, std_err = stats.linregress(x3,np.log10(HG_T[2][exp]))
HG_T_4s, HG_T_4_i, r_value, p_value, std_err = stats.linregress(x4,np.log10(HG_T[3][exp]))
LG_T_1s, LG_T_1_i, r_value, p_value, std_err = stats.linregress(x1,np.log10(LG_T[0][exp]))
LG_T_2s, LG_T_2_i, r_value, p_value, std_err = stats.linregress(x1,np.log10(LG_T[1][exp]))
LG_T_3s, LG_T_3_i, r_value, p_value, std_err = stats.linregress(x3,np.log10(LG_T[2][exp]))
LG_T_4s, LG_T_4_i, r_value, p_value, std_err = stats.linregress(x4,np.log10(LG_T[3][exp]))

fit_HG_1 = np.asarray(x1)*HG_T_1s+HG_T_1_i
fit_HG_2 = np.asarray(x1)*HG_T_2s+HG_T_2_i
fit_HG_3 = np.asarray(x3)*HG_T_3s+HG_T_3_i
fit_HG_4 = np.asarray(x4)*HG_T_4s+HG_T_4_i
fit_LG_1 = np.asarray(x1)*LG_T_1s+LG_T_1_i
fit_LG_2 = np.asarray(x1)*HG_T_2s+HG_T_2_i
fit_LG_3 = np.asarray(x3)*LG_T_3s+LG_T_3_i
fit_LG_4 = np.asarray(x4)*LG_T_4s+LG_T_4_i

#print(HG_T_1s,HG_T_1_i)
#print(HG_T[0][3])

fig1 = plt.figure(figsize=(34,16))
ax1 = fig1.add_subplot(2,2,1)
ax2 = fig1.add_subplot(2,2,3)

ax1.semilogy(x1,HG_T[0][exp], "r",  label = "Sensor 1")
ax1.semilogy(x2,HG_T[1][exp], "y",  label = "Sensor 2")
ax1.semilogy(x3,HG_T[2][exp], "g",  label = "Sensor 3")
ax1.semilogy(x4,HG_T[3][exp], "b",  label = "Sensor 4")

ax1.text(30,100,"S1 Td ="+ str(round(np.log10(2)/HG_T_1s,1)))
ax1.text(30,60,"S2 Td ="+ str(round(np.log10(2)/HG_T_2s,1)))
ax1.text(30,300,"S3 Td ="+ str(round(np.log10(2)/HG_T_4s,1)))
ax1.text(30,170,"S4 Td ="+ str(round(np.log10(2)/HG_T_3s,1)))

ax2.semilogy(x1,LG_T[0][exp], "r", label = "Sensor 1")
ax2.semilogy(x2,LG_T[1][exp], "y", label = "Sensor 2")
ax2.semilogy(x3,LG_T[2][exp], "g", label = "Sensor 3")
ax2.semilogy(x4,LG_T[3][exp], "b", label = "Sensor 4")

ax2.text(30,6,"S1 Td ="+ str(round(np.log10(2)/LG_T_1s,1)))
ax2.text(30,3,"S2 Td ="+ str(round(np.log10(2)/LG_T_2s,1)))
ax2.text(30,20,"S3 Td ="+ str(round(np.log10(2)/LG_T_4s,1)))
ax2.text(30,10,"S4 Td ="+ str(round(np.log10(2)/LG_T_3s,1)))


ax1.set_xlabel("Temperature (C)",fontsize=12)
ax1.set_ylabel("ADU",fontsize=12,labelpad=10)
ax1.set_title("HG Dark Current against temperture",fontsize=16)
ax1.grid(True,which='both')
ax1.legend()
ax2.set_xlabel("Temperature (C)",fontsize=12)
ax2.set_ylabel("ADU",fontsize=12,labelpad=10)
ax2.set_title("LG Dark Current against temperture",fontsize=16)
ax2.grid(True,which='both')
ax2.legend()

plt.show()
# Calculate the logarith amd stuff

ui = 27
T = 36
tref = 26
uref = 15.8

Td = (T-tref)/(np.log2(ui)-np.log2(uref))
#print(Td)


end = time.time()
print("Code took")
print(round(end - start, 3))
print("seconds")
   




