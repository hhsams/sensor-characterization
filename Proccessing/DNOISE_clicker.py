#!/usr/bin/env python
import pyautogui
import time
import numpy as np
import pyperclip as pc

def write_registers():
	pyautogui.click(759,71)					#Click on "DEBUG"
	pyautogui.click(808,873)				#Click on "Write sensor registers"
	time.sleep(1)					#Wait for write success
	pyautogui.click(808,833)                #Click on "READ SENSOR REGISTERS"
	time.sleep(1)
	pyautogui.click(638,71)					#Click on "IMAGE"

def set_exposure(exposure):
	pyautogui.click(65,100)					#Click on FPGA
	pyautogui.doubleClick(209,175) 	        #Click on int time value
	pc.copy(exposure)                       #Copy exposure value
	pyautogui.hotkey("ctrl","v")	        #Paste int time
	pyautogui.press('enter')
	write_registers()

def set_gains(BOT_gain, TOP_gain):
	pyautogui.click(377,100)                #Click on Function
	time.sleep(0.2)
	pyautogui.click(377,100)                #Click on Function
	time.sleep(0.2)
	pc.copy(BOT_gain)                       #Copy high gain
	pyautogui.doubleClick(216,685)          #Click on PGA_GAIN_BOT value
	time.sleep(0.2)
	pyautogui.hotkey("ctrl","v")
	time.sleep(0.2)
	pyautogui.press('enter')
	pc.copy(TOP_gain)                       #Copy high gain
	pyautogui.doubleClick(216,616)          #Click on PGA_GAIN_TOP value
	time.sleep(0.2)
	pyautogui.hotkey("ctrl","v")
	time.sleep(0.2)
	pyautogui.press('enter')

def set_frames(n_frames):
	pyautogui.click(65,100) 				#Click on FPGA
	time.sleep(0.2)
	pyautogui.doubleClick(218,310) 	        #Click on frames value
	time.sleep(0.2)
	pc.copy(n_frames)                       #Copy frame nr
	time.sleep(0.2)
	pyautogui.hotkey("ctrl","v")	        #Paste frames
	pyautogui.press('enter')
	
def save_image(path, filename,img_save_time):
	pyautogui.click(638,71) 				#Click on "IMAGE"
	time.sleep(0.2)
	pyautogui.click(1500,871) 				#Click on "SAVE RAW"
	time.sleep(0.2)
	pc.copy(path+"\\"+filename)
	pyautogui.hotkey("ctrl","v")
	time.sleep(0.2)
	pyautogui.press('enter')
	time.sleep(img_save_time)

def get_fileName(exposure,gain,temperature):#format: EXP_GAIN_TEMP
	return str(exposure)+"_"+str(gain)+"_"+str(temperature)

def take_image(img_shoot_time):
    pyautogui.click(851,873)	#Click on "RUN ONCE"
    time.sleep(img_shoot_time)


def main():
	img_save_time = 8		#Global for all exposure times. Perhaps should calculate how long each exposure time takes
	bias_shoot_time = 5
	frames = 10
	bias_frames = 50
	temperature = "41.0"
	sensor_nr = "Two"
	TOP_gain = 2			#Low gain
	BOT_gain = 38			#High gain
	exposures = [35714,53571,71428,89285,107142,125000] #400,600,800,1000,1200,1400(ms)
	image_index=0

	dark_path = "F:\\TheiaStuff\\Sensori_mootmised\\Sensor" + sensor_nr + "\\Raw" + "\\DarkCurrent" + "\\" + temperature
	bias_path = "F:\\TheiaStuff\\Sensori_mootmised\\Sensor" + sensor_nr + "\\Raw" + "\\Bias" + "\\" + temperature
	gain_string = str(TOP_gain) + "_" + str(BOT_gain)

	set_gains(BOT_gain,TOP_gain)
	set_frames(frames)
	#---------- SENSOR WARMUP -------------
	print("==============SENSOR WARMUP==============")
	for exp_time in exposures:
		print("=========================================")
		image_index += 1
		img_shoot_time = ((exp_time*0.0112+0.0056))/100+2
		set_exposure(exp_time)
		print("Exposure time set to: ", exp_time, "clock cycles")

		take_image(img_shoot_time)
		print("Image taken")

		print("Done ",image_index, " out of ", len(exposures))
	#---------- BIAS FRAME -------
	print("================BIAS FRAME===============")
	bias_filename = get_fileName(0,gain_string,temperature)
	set_frames(bias_frames)
	set_exposure(0)
	take_image(bias_shoot_time)
	save_image(bias_path,bias_filename,img_save_time*5)

	print("Bias image successful")

	#------------- IMAGES -------------
	image_index = 0
	set_frames(frames)
	print("==============TAKING IMAGES==============")
	for exp_time in exposures:
		print("=========================================")
		start_time = time.time()

		image_index += 1
		img_shoot_time = (((exp_time*0.0112+0.0056))/1000)*10+2
		
		filename = get_fileName(exp_time,gain_string,temperature)

		set_exposure(exp_time)
		print("Exposure time set to: ", exp_time, "clock cycles")

		take_image(img_shoot_time)
		print("Image taken")

		save_image(dark_path,filename,img_save_time)
		print("Image saved as ", dark_path + "\\" + filename)

		print("Done ",image_index, " out of ", len(exposures))
		print("Time elapsed: ", round(time.time()-start_time,2), "s")

if __name__ == "__main__":
	main()
