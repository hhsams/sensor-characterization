# All the base functions required to analyse ESEO secondary camera sensor data
from astropy.io import fits
import numpy as np
import os

### Frame information functions ###

#Zero bias frame information

def get_ZBframe_info(filename):
    filename = filename.split('_')
    gain = filename[3]
    
    return gain

# Linearity frame information
    
def get_LINframe_info(filename,extension):
    filename = filename.split('_')
    gain = filename[2]
    shutter = filename[3].replace(extension,'')
    
    return gain,shutter

# Dark current frame information
    
def get_DCframe_info(filename,extension):
    filename = filename.split('_')
    if extension == '.txt':
        gain = filename[2]
        shutter = filename[3].replace('.txt','')
        return gain,shutter
    if extension == '.fit':
        temp = filename[1].replace('C','')
        gain = filename[3]
        shutter = filename[4].replace('.fit','')
        return temp, gain, shutter  


### Array operation functions  ###
    
# Addition of 2 frames

def add_frame(base,frame,filename):
    print(filename)
    return np.add(base,frame)

# Substraction of 2 frames
    
def substract_frame(base, frame, filename1, filename2):
    print("Substracting: "+filename1+" from "+filename2)
    return np.subtract(base,frame)

# Use only the middle square of the image
    
def middle(array,shape,lenght):
    middley = int(shape[0]/2)
    middlex = int(shape[1]/2)
    rowRange = (middley-lenght,middley+lenght)
    colRange = (middlex-lenght,middlex+lenght)
    newArray = array[rowRange[0]:rowRange[1],colRange[0]:colRange[1]]
    return newArray


def find_DC_frame(directory,gain,shutter):
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".txt"):
            DCgain,DCshutter = get_DCframe_info(filename)
            if DCgain == gain and DCshutter == shutter:
                return filename
