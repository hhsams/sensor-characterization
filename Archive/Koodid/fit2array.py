import numpy as np
from astropy.io import fits
import os
import functions as func


LIN_path_fits ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Fits\\"
LIN_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
LIN_DC_path_fits ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\Fits\\"
DC_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\"

frameShape = (1944, 2592)
l = 256
directory = os.fsencode(LIN_path_fits)


doneFrames = []
"""
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        gain,shutter = func.get_LINframe_info(filename,'.fit1.fit')
        if gain+shutter not in doneFrames:
            image_data = func.middle(fits.getdata(LIN_path_fits+filename),frameShape,l//2)

            print("Current gain: "+gain)
            np.savetxt(LIN_path+"LIN_"+gain+"_"+shutter+".txt",image_data, delimiter='\t')
            doneFrames.append(gain+shutter)
        else:
            image_data = func.middle(fits.getdata(LIN_path_fits+filename),frameShape,l//2)
            print("Current gain: "+gain)
            np.savetxt(LIN_path+"ALIN_"+gain+"_"+shutter+".txt",image_data, delimiter='\t')
"""
directory = os.fsencode(LIN_DC_path_fits)

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        gain,shutter = func.get_LINframe_info(filename,'.fit1.fit')
        if gain+shutter not in doneFrames:
            image_data = func.middle(fits.getdata(LIN_DC_path_fits+filename),frameShape,l//2)

            print("Current gain: "+gain)
            np.savetxt(DC_path+"LIN_"+gain+"_"+shutter+".txt",image_data, delimiter='\t')
            doneFrames.append(gain+shutter)
        else:
            image_data = func.middle(fits.getdata(LIN_DC_path_fits+filename),frameShape,l//2)
            print("Current gain: "+gain)
            np.savetxt(DC_path+"ALIN_"+gain+"_"+shutter+".txt",image_data, delimiter='\t')