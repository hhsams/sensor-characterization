import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

LIN_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Data\\"
DC_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\Data\\"

f = open(LIN_path+'statdata.txt')
f.readline() # Skip first line

#g1 = [[shutter],[median],[mean],[variance],[standard dev]]
g1 = [[],[],[],[],[]]
g2 = [[],[],[],[],[]]
g3 = [[],[],[],[],[]]
g4 = [[],[],[],[],[]]

for row in f:
    row = row.split('\t')
    if row[1] == '1':
        g1[0].append(1/float(row[0])), g1[1].append(float(row[2])), g1[2].append(float(row[3].replace('\n','')))
    if row[1] == '2':
        g2[0].append(1/float(row[0])), g2[1].append(float(row[2])), g2[2].append(float(row[3].replace('\n','')))
    if row[1] == '3':
        g3[0].append(1/float(row[0])), g3[1].append(float(row[2])), g3[2].append(float(row[3].replace('\n','')))
    if row[1] == '4':
        g4[0].append(1/float(row[0])), g4[1].append(float(row[2])), g4[2].append(float(row[3].replace('\n','')))

f.close()
print(g1[1][0])
f = open(DC_path+'statdata.txt')
f.readline()
i = 0
for row in f:
    row = row.split('\t')
    if row[1] == '1':
        g1[1][i] = g1[1][i] - float(row[2])
        g1[2][i] = g1[2][i] - float(row[3])
    if row[1] == '2':
        g2[1][i-50] = g2[1][i-50] - float(row[2])
        g2[2][i-50] = g2[2][i-50] - float(row[3])
    if row[1] == '3':
        g3[1][i-100] = g3[1][i-100] - float(row[2])
        g3[2][i-100] = g3[2][i-100] - float(row[3])
    if row[1] == '4':
        g4[1][i-150] = g4[1][i-150] - float(row[2])
        g4[2][i-150] = g4[2][i-150] - float(row[3])
    i+=1
f.close()

g1_s, intercept, r_value, p_value, std_err = stats.linregress(g1[1][3:],g1[2][3:])
g2_s, intercept2, r_value, p_value, std_err = stats.linregress(g2[1][20:],g2[2][20:])
g3_s, intercept, r_value, p_value, std_err = stats.linregress(g3[1][6:],g3[2][6:])
g4_s, intercept, r_value, p_value, std_err = stats.linregress(g4[1][6:],g4[2][6:])
print(1/g1_s)
print(1/g2_s)
print(1/g3_s)
print(1/g4_s)

fit_g2 = np.asarray(g2[1][6:])*g2_s+intercept2
dif_g2 = np.divide(np.subtract(np.asarray(g2[1][6:]),fit_g2),fit_g2)

fig1 = plt.figure(figsize=(16,10))
ax1 = fig1.add_subplot(2,2,1)
ax2 = fig1.add_subplot(2,2,2)
ax3 = fig1.add_subplot(2,2,3)
ax4 = fig1.add_subplot(2,2,4)


ax1.plot(g2[1][6:],dif_g2,'ro', label="")
ax2.plot(g2[1][6:],g2[2][6:],'ro', label="")
ax2.plot(g2[1][6:],fit_g2, label="")
ax3.plot(g3[1][6:],g3[2][6:],'ro', label="")
ax4.plot(g4[1][6:],g4[2][6:],'ro',label="")

ax1.set_xlabel("$\mu_{gray} - \mu_{dark}$",fontsize=12)
ax1.set_ylabel("$\sigma_{gray}^2 - \sigma_{dark}^2$",fontsize=12,labelpad=10)
ax2.set_xlabel("$\mu_{gray} - \mu_{dark}$",fontsize=12)
ax2.set_ylabel("$\sigma_{gray}^2 - \sigma_{dark}^2$",fontsize=12,labelpad=10)
ax3.set_xlabel("$\mu_{gray} - \mu_{dark}$",fontsize=12)
ax3.set_ylabel("$\sigma_{gray}^2 - \sigma_{dark}^2$",fontsize=12,labelpad=10)
ax4.set_xlabel("$\mu_{gray} - \mu_{dark}$",fontsize=12)
ax4.set_ylabel("$\sigma_{gray}^2 - \sigma_{dark}^2$",fontsize=12,labelpad=10)

ax1.set_title("Gain 1")
ax2.set_title("Gain 2")
ax3.set_title("Gain 3")
ax4.set_title("Gain 4")

#ax1.text(1000,1750,"slope ="+ str(round(g1_s,3)))
ax2.text(1000,3500,"slope ="+ str(round(g2_s,3)))
ax3.text(1000,5000,"slope ="+ str(round(g3_s,3)))
ax4.text(1000,6000,"slope ="+ str(round(g4_s,3)))

ax1.legend()

