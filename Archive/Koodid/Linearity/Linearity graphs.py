import numpy as np
import matplotlib.pyplot as plt


path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Processed\\Data\\"

f = open(path+'mediandata.txt')
f.readline() # Skip first line

#g1 = [[shutter],[median],[mean],[variance],[standard dev]]
g1 = [[],[],[],[],[]]
g2 = [[],[],[],[],[]]
g3 = [[],[],[],[],[]]
g4 = [[],[],[],[],[]]

for row in f:
    row = row.split('\t')
    if row[1] == '1':
        g1[0].append(1/float(row[0])), g1[1].append(row[2]), g1[2].append(row[3]), g1[3].append(row[4]), g1[4].append(row[5].replace('\n',''))
    if row[1] == '2':
        g2[0].append(1/float(row[0])), g2[1].append(row[2]), g2[2].append(row[3]), g2[3].append(row[4]), g2[4].append(row[5].replace('\n',''))
    if row[1] == '3':
        g3[0].append(1/float(row[0])), g3[1].append(row[2]), g3[2].append(row[3]), g3[3].append(row[4]), g3[4].append(row[5].replace('\n',''))
    if row[1] == '4':
        g4[0].append(1/float(row[0])), g4[1].append(row[2]), g4[2].append(row[3]), g4[3].append(row[4]), g4[4].append(row[5].replace('\n',''))

for i in range(4):
    g1[i] = [float(j) for j in g1[i]]
    g2[i] = [float(j) for j in g2[i]]
    g3[i] = [float(j) for j in g3[i]]
    g4[i] = [float(j) for j in g4[i]]



fit_g1 = np.polyfit(g1[0],g1[1],1)
fit_fn1 = np.poly1d(fit_g1) 
fit_g2 = np.polyfit(g2[0],g2[1],1)
fit_fn2 = np.poly1d(fit_g2) 
fit_g3 = np.polyfit(g3[0],g3[1],1)
fit_fn3 = np.poly1d(fit_g3) 
#fit_g4 = np.polyfit(g4[0],g4[1],1)
#fit_fn4 = np.poly1d(fit_g4) 

dif1 = []
dif2 = []
dif3 = []
#dif4 = []
for i in range(len(g1[0])):
     dif1.append(-(fit_g1[0]*g1[0][i]+fit_g1[1]) + g1[1][i])
     dif2.append(-(fit_g2[0]*g2[0][i]+fit_g2[1]) + g2[1][i])
     dif3.append(-(fit_g3[0]*g3[0][i]+fit_g3[1]) + g3[1][i])
#     dif4.append(fit_g4[0]*g4[0][i]+fit_g4[1] - g4[1][i])


fig1 = plt.figure(figsize=(16,10))
ax1 = fig1.add_subplot(2,2,1)
ax2 = fig1.add_subplot(2,2,2)
ax3 = fig1.add_subplot(2,2,3)
ax4 = fig1.add_subplot(2,2,4)
fig2 = fig1 = plt.figure(figsize=(16,10))
ax5 = fig2.add_subplot(2,2,1)
ax6 = fig2.add_subplot(2,2,2)
ax7=  fig2.add_subplot(2,2,3)
ax8 = fig2.add_subplot(2,2,4)

ax1.plot(g1[0],g1[1],'ro',g1[0],fit_fn1(g1[0]), label="")
ax2.plot(g1[0],dif1,'ro', label="")
ax3.plot(g2[0],g2[1],'ro',g2[0],fit_fn2(g2[0]), label="")
ax4.plot(g2[0],dif2,'ro',label="")
ax5.plot(g3[0],g3[1],'ro',g3[0],fit_fn3(g3[0]),label="")
ax6.plot(g3[0],dif3,'ro',label="")
#ax7.plot(g4[0],g4[1],'ro',g4[0],fit_fn(g4[0]),label="")
#ax8.plot(g4[0],dif4,'ro',label="")
ax1.set_xlabel("Exposure (s)",fontsize=16)
ax1.set_ylabel("ADU",fontsize=16,labelpad=10)

ax1.legend()