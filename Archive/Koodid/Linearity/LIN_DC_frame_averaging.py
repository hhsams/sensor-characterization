import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os

LIN_DC_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\"

nr_of_frames = 2
frameShape = (1944, 2592)

def get_Linframe_info(filename):
    filename = filename.split('_')
    gain = filename[3]
    shutter = filename[4].replace('.fit','')
    
    return gain,shutter

def add_frame(base,path,filename):
    print(filename)
    image_data = fits.getdata(path+filename)
    image_data = middle(image_data, frameShape)
    base = np.add(base,image_data)
    return base

def middle(array,shape):
    middley = int(shape[0]/2)
    middlex = int(shape[1]/2)
    rowRange = (middley-128,middley+128)
    colRange = (middlex-128,middlex+128)
    #print(rowRange,colRange)
    newArray = array[rowRange[0]:rowRange[1],colRange[0]:colRange[1]]
    return newArray
    
#Read Zero bias files and find the means for each gains
directory = os.fsencode(LIN_DC_path)
base = np.zeros((256,256))
n = nr_of_frames-1

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"): 
        if n == 0:
            base = add_frame(base,LIN_DC_path,filename)
            gain,shutter = get_Linframe_info(filename)
            print("Current gain: "+gain)
            base = np.round(np.divide(base,nr_of_frames))
            np.savetxt(LIN_DC_path+"mean_LINDC_"+gain+"_"+shutter+".txt",base, delimiter='\t')
            base = np.zeros((256,256))
            n = nr_of_frames
        else:
            base = add_frame(base,LIN_DC_path,filename)
        n -= 1

        