import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import functions as func

LIN_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
DC_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\"


l = 256
#Open text file where to save the statistical data
f = open(LIN_path+"Data\\"+"statdata.txt","w")
f.write("Shutter\tGain\tMean\tVariance\n")

directory = os.fsencode(LIN_path)

frameNames = []
doneFrames = []

mean = 0
var = 0
         
for file in os.listdir(directory):
    filename1 = os.fsdecode(file)
    if filename1.endswith(".txt"):
        gain,shutter = func.get_LINframe_info(filename1,'.txt')
        if gain+shutter not in doneFrames:
            frameNames.append(filename1)
            #print(frameNames,1)
            for file in os.listdir(directory):
                if filename1 != os.fsdecode(file):
                    filename2 = os.fsdecode(file)
                    if filename2.endswith(".txt"):
                        gain2,shutter2 = func.get_LINframe_info(filename2,'.txt')
                        if gain == gain2 and shutter == shutter2:
                            frameNames.append(filename2)
                            #print(frameNames,2)
            
            frame1 = np.loadtxt(LIN_path+frameNames[0])
            frame2 = np.loadtxt(LIN_path+frameNames[1])        
            for i in range(0,255):
                for j in range(0,255):
                    mean += (frame1[i][j]+frame2[i][j])
                    var += (frame1[i][j]-frame2[i][j])**2
            mean = mean/(2*l**2)
            var = var/(2*l**2)
                    
            f.write(shutter+'\t'+gain+'\t'+str(mean)+'\t'+str(var)+'\n')
                
            doneFrames.append(gain+shutter)
            print(frameNames)
            frameNames = []
            mean = 0
            var = 0
f.close()
         
f = open(DC_path+"Data\\"+"statdata.txt","w")
f.write("Shutter\tGain\tMean\tVariance\n")

directory = os.fsencode(DC_path)

frameNames = []
doneFrames = []
         
for file in os.listdir(directory):
    filename1 = os.fsdecode(file)
    if filename1.endswith(".txt"):
        gain,shutter = func.get_LINframe_info(filename1,'.txt')
        if gain+shutter not in doneFrames:
            frameNames.append(filename1)
            for file in os.listdir(directory):
                if filename1 != os.fsdecode(file):
                    filename2 = os.fsdecode(file)
                    if filename2.endswith(".txt"):
                        gain2,shutter2 = func.get_LINframe_info(filename2,'.txt')
                        if gain == gain2 and shutter == shutter2:
                            frameNames.append(filename2)
            
            frame1 = np.loadtxt(DC_path+frameNames[0])
            frame2 = np.loadtxt(DC_path+frameNames[1])        
            for i in range(0,255):
                for j in range(0,255):
                    mean += (frame1[i][j]+frame2[i][j])
                    var += (frame1[i][j]-frame2[i][j])**2
            mean = mean/(2*l**2)
            var = var/(2*l**2)
                    
            f.write(shutter+'\t'+gain+'\t'+str(mean)+'\t'+str(var)+'\n')
            doneFrames.append(gain+shutter)
            print(frameNames)
            frameNames = []
            mean = 0
            var = 0
         

f.close()