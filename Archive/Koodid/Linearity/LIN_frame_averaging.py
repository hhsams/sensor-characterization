import numpy as np
from astropy.io import fits
import os
import functions as func

LIN_path_fits ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Fits\\"
LIN_DC_path_fits ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\Fits\\"
LIN_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
DC_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\"

nr_of_frames = 2
frameShape = (1944, 2592)
l = 256
    
#Read Zero bias files and find the means for each gains
directory = os.fsencode(LIN_path_fits)
base = np.zeros((l,l))
n = nr_of_frames-1

frameNames = []
doneFrames = []

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        gain,shutter = func.get_LINframe_info(filename,'.fit1.fit')
        if gain+shutter not in doneFrames:
            frameNames.append(filename)
            for file in os.listdir(directory):
                if filename != os.fsdecode(file):
                    filename = os.fsdecode(file)
                    if filename.endswith(".fit"):
                        gain2,shutter2 = func.get_LINframe_info(filename,'.fit1.fit')
                        if gain == gain2 and shutter == shutter2:
                            frameNames.append(filename)
            for i in range(nr_of_frames):
                image_data = func.middle(fits.getdata(LIN_path_fits+frameNames[i]),frameShape,l//2)
                base = func.add_frame(base,image_data,frameNames[i])
           
            print("Current gain: "+gain)
            base = np.round(np.divide(base,nr_of_frames))
            np.savetxt(LIN_path+"meanLIN_"+gain+"_"+shutter+".txt",base, delimiter='\t')
            base = np.zeros((l,l))
            doneFrames.append(gain+shutter)
            frameNames = []
           
# LINDC frames averaging
       
directory = os.fsencode(LIN_DC_path_fits)
base = np.zeros((l,l))
n = nr_of_frames-1
frameNames = []
doneFrames = []

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        gain,shutter = func.get_LINframe_info(filename,'.fit1.fit')
        if gain+shutter not in doneFrames:
            frameNames.append(filename)
            for file in os.listdir(directory):
                if filename != os.fsdecode(file):
                    filename = os.fsdecode(file)
                    if filename.endswith(".fit"):
                        gain2,shutter2 = func.get_LINframe_info(filename,'.fit1.fit')
                        if gain == gain2 and shutter == shutter2:
                            frameNames.append(filename)
            for i in range(nr_of_frames):
                image_data = func.middle(fits.getdata(LIN_DC_path_fits+frameNames[i]),frameShape,l//2)
                base = func.add_frame(base,image_data,frameNames[i])
           
            print("Current gain: "+gain)
            base = np.round(np.divide(base,nr_of_frames))
            np.savetxt(DC_path+"mean_LINDC_"+gain+"_"+shutter+".txt",base, delimiter='\t')
            base = np.zeros((256,256))
            doneFrames.append(gain+shutter)
            frameNames = []
        
        


        