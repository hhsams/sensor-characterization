import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import functions as func

LIN_processed_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Processed\\"

f = open(LIN_processed_path+'Data\\'+"mediandata.txt","w")
f.write("Shutter\tGain\tMedian\tMean\tVariance\tStd\n")

directory = os.fsencode(LIN_processed_path)

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"): 
        frame = np.loadtxt(LIN_processed_path+filename)
        print(filename)
        gain,shutter = func.get_LINframe_info(filename,'.txt')
        mean = str(np.mean(frame))
        variance = str(np.var(frame))
        print(variance)
        variance2 = str()
        std = str(np.std(frame))
        median = str(np.median(frame))
        print(mean,variance,std)
        f.write(shutter+'\t'+gain+'\t'+median+'\t'+mean+'\t'+variance+'\t'+std+'\n')
f.close()