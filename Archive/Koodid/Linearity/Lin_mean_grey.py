import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
LIN_path_G1 ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\MeanG1\\"
LIN_path_G2 ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\MeanG2\\"
LIN_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
directories = [LIN_path_G1,LIN_path_G2]

def get_Linframe_info(filename):
    filename = filename.split('_')
    gain = filename[2]
    shutter = filename[3].replace('.txt','')
    
    return gain,shutter

f = open(LIN_path+"data.txt","w")
f.write("Shutter\tGain\tMean\tVariance\tStd\n")

for i in directories:
    for file in os.listdir(i):
        filename = os.fsdecode(file)
        if filename.endswith(".txt"): 
            gain,shutter = get_Linframe_info(filename) 
            frame = np.loadtxt(i+filename)
            print(filename)
            mean = np.mean(frame)
            variance = np.var(frame)
            std = np.std(frame)
            f.write(shutter+'\t'+gain+'\t'+str(mean)+'\t'+str(variance)+'\t'+str(std)+'\t\n')
            print(mean,variance,std)
f.close()