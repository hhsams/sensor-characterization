import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import sys
#import functions


dark_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Dark_frames\\"
lin_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Lin_frames\\"

def get_DCframe_info(filename):
    filename = filename.split('_')
    gain = filename[1]
    shutter = filename[3].replace('.fit1.fit','')
    return gain, shutter  

def middle(array,shape,lenght):
    middley = int(shape[0]/2)
    middlex = int(shape[1]/2)
    rowRange = (middley-lenght,middley+lenght)
    colRange = (middlex-lenght,middlex+lenght)
    newArray = array[rowRange[0]:rowRange[1],colRange[0]:colRange[1]]
    return newArray


# First lets find the mean gray and variance value of each dark frame
        
path = "E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
frameShape = (1944, 2592)
l = 256
"""
f = open(path+'darkAverage.txt','w')

f.write("Gain\tShutter\tValue\tVariance\n")

doneExposures = []
for file in os.listdir(dark_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):

        gain,shutter = get_DCframe_info(filename)
        
        if shutter+gain not in doneExposures:
            print('Adding dark, '+shutter+' Hz')
            doneExposures.append(shutter+gain)
            darkSum = np.mean(middle(fits.getdata(dark_path + filename),frameShape,l))
            darkVar = np.var(middle(fits.getdata(dark_path + filename),frameShape,l))
            
            for file in os.listdir(dark_path):
                filename1 = os.fsdecode(file)
                if filename1 != filename:
                    gain1,shutter1 = get_DCframe_info(filename1)
                    
                    if shutter1 == shutter and gain1 == gain:
                        #print(filename1)
                        darkSum += np.mean(middle(fits.getdata(dark_path + filename1),frameShape,l))
                        darkVar += np.var(middle(fits.getdata(dark_path + filename1),frameShape,l))
            darkAverage = darkSum/2
            darkVariance = darkVar/2
            f.write(str(gain)+'\t'+str(shutter)+'\t'+str(darkAverage)+'\t'+str(darkVariance)+'\n')
f.close()
"""
# Next, lets do the same for linearity

f = open(path+'linAverage.txt','w')

f.write("Gain\tShutter\tValue\tVariance\n")

doneExposures = []
for file in os.listdir(lin_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):

        gain,shutter = get_DCframe_info(filename)
        
        if shutter+gain not in doneExposures:
            print('Adding lin, '+shutter+' Hz'+ ', '+gain)
            doneExposures.append(shutter+gain)
            linSum = np.mean(middle(fits.getdata(lin_path + filename),frameShape,l))
            linVar = np.var(middle(fits.getdata(lin_path + filename),frameShape,l))
            
            for file in os.listdir(lin_path):
                filename1 = os.fsdecode(file)
                if filename1 != filename:
                    gain1,shutter1 = get_DCframe_info(filename1)
                    
                    if shutter1 == shutter and gain1 == gain:
                        print(filename1)
                        linSum += np.mean(middle(fits.getdata(lin_path + filename1),frameShape,l))
                        linVar += np.var(middle(fits.getdata(lin_path + filename1),frameShape,l))
            linAverage = linSum/2
            linVariance = linVar/2
            f.write(str(gain)+'\t'+str(shutter)+'\t'+str(linAverage)+'\t'+str(linVariance)+'\n')
            
            
            
            







