import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
DC_path = "E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Dark current\\Uus dark\\"
ZB_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Zero bias\\Zero uus\\"

Zbiasframenr = 20-1
frameShape = (1944, 2592)

def get_ZBframe_info(filename):
    filename = filename.split('_')
    framenr = int(filename[2])
    gain = filename[3]
    
    return framenr, gain

def get_DCframe_info(filename):
    filename = filename.split('_')
    temp = int(filename[1].replace('C',''))
    framenr = int(filename[2])
    gain = int(filename[3])
    shutter = int(filename[4].replace('.fit',''))
    
    return temp, framenr, gain, shutter
    
#Read Zero bias files and find the means for each gains
directory = os.fsencode(ZB_path)
base = np.zeros(frameShape)
n = Zbiasframenr
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"): 
        if n == 0:
            print(filename)
            image_data = fits.getdata(ZB_path+filename)
            ase = np.add(base,image_data)
            framenr, gain = get_ZBframe_info(filename)
            print(gain)
            base = np.round(np.divide(base,Zbiasframenr))
            np.savetxt(ZB_path+"mean_ZB_"+gain+".txt",base, delimiter='\t')
            base = np.zeros(frameShape)
            n = Zbiasframenr
        print(filename)
        image_data = fits.getdata(ZB_path+filename)
        base = np.add(base,image_data)
        n -= 1
         #plt.imshow(image_data, cmap='gray')
         #plt.colorbar()
        #plt.imsave(path2+filename+".png",image_data,cmap='gray',vmin=10,vmax=700)
        # plt.show() .
        
#Find the mean gray and variance for each gain
directory = os.fsencode(ZB_path)
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"): 
        frame = np.loadtxt(ZB_path+filename)
        