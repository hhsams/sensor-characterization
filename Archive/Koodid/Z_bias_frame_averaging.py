import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os

ZB_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Zero bias\\Zero uus\\"

nr_of_frames = 20-1
frameShape = (1944, 2592)

def get_ZBframe_info(filename):
    filename = filename.split('_')
    gain = filename[3]
    
    return gain

def add_frame(base,path,filename):
    print(filename)
    image_data = fits.getdata(path+filename)
    base = np.add(base,image_data)
    return base
    
#Read Zero bias files and find the means for each gains
directory = os.fsencode(ZB_path)
base = np.zeros(frameShape)
n = nr_of_frames

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"): 
        if n == 0:
            base = add_frame(base,ZB_path,filename)
            gain = get_ZBframe_info(filename)
            print("Current gain: "+gain)
            base = np.round(np.divide(base,nr_of_frames))
            np.savetxt(ZB_path+"mean_ZB_"+gain+".txt",base, delimiter='\t')
            base = np.zeros(frameShape)
            n = nr_of_frames
        else:
            base = add_frame(base,ZB_path,filename)
        n -= 1

        