import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import sys

def get_dark_info(filename):
    filename = filename.split('_')
    shutter = filename[2].replace('.fit','')
    return shutter

def get_spec_info(filename):
    filename = filename.split('_')
    shutter = filename[2]
    wavelenght = filename[3].replace('.fit','')
    count = filename[1]
    return wavelenght,shutter,count

def get_avrg_info(filename):
    filename = filename.split('_')
    shutter = filename[1]
    wavelenght = filename[2].replace('.fit','')
    return wavelenght,shutter

dark_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Dark_frames\\"
spec_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Spec_frames\\"


# First lets average all the DC frames together

doneExposures = []
frameCount = 10
"""
for file in os.listdir(dark_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):

        shutter = get_dark_info(filename)
        
        if shutter not in doneExposures:
            print('Adding dark, '+shutter+' Hz')
            doneExposures.append(shutter)
            darkSum = fits.getdata(dark_path + filename)
            
            for file in os.listdir(dark_path):
                filename = os.fsdecode(file)
                if filename.endswith(".fit"):
                    shutter1 = get_dark_info(filename)
                    
                    if shutter1 == shutter:
                        #print(filename)
                        darkSum = np.add(darkSum,fits.getdata(dark_path + filename))
            darkMaster = np.uint16(darkSum/frameCount)
            #np.savetxt(dark_path+"Master_frames\\"+"darkMaster_"+shutter+".txt",darkMaster, delimiter='\t')
            hdu = fits.PrimaryHDU(darkMaster)
            hdu.writeto(dark_path+"Master_frames\\"+"darkMaster_"+shutter+".fit", overwrite=True)
        
# Now lets subtract the master dark frames from each frame
        
spec_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Spec_frames\\"

for file in os.listdir(dark_path+"Master_frames\\"):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        shutter = filename.split("_")[1].replace(".fit","")
        darkFrame = fits.getdata(dark_path + "Master_frames\\" + filename)
        print('Subtracting dark, '+shutter+' Hz')
        for file in os.listdir(spec_path):
            filename = os.fsdecode(file)
            if filename.endswith(".fit"):
                wavelenght,shutter1,count = get_spec_info(filename)
                
                if shutter1 == shutter:
                    #print(filename)
                    specFrame = fits.getdata(spec_path + filename)
                    processedFrame = np.uint16(specFrame - darkFrame)
                    hdu = fits.PrimaryHDU(processedFrame)
                    hdu.writeto(spec_path+"Processed_frames\\"  +"cleanSpec_" + count + "_" + shutter1 + "_" + wavelenght + ".fit", overwrite=True)

# Next, get the averages of all the spec frames for each wavelenght
doneFrames = []
spec_proc_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Spec_frames\\Processed_frames\\"
                    
for file in os.listdir(spec_proc_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        wavelenght,shutter,count = get_spec_info(filename)
        if wavelenght+shutter not in doneFrames:
            print('Averaging, '+wavelenght+' nm and '+shutter+' Hz')
            doneFrames.append(wavelenght+shutter)
            specSum = np.uint16(fits.getdata(spec_proc_path + filename))
            
            for file in os.listdir(spec_proc_path):
                if filename != os.fsdecode(file):
                    filename1 = os.fsdecode(file)
                    
                    if filename1.endswith(".fit"):
                        wavelenght1,shutter1,count1 = get_spec_info(filename1)
                        
                        if wavelenght == wavelenght1 and shutter == shutter1:
                            frame = np.uint16(fits.getdata(spec_proc_path + filename1))
                            specSum = frame + specSum
                            
            specMaster = np.uint16(specSum/frameCount)
            hdu = fits.PrimaryHDU(specMaster)
            hdu.writeto(spec_proc_path+"Averaged_frames\\"+"avrg_"+shutter+'_'+wavelenght+".fit", overwrite=True)

# Next, adjust all the frames to match exposure to 300 Hz or 0.003333 ms
            
spec_avrg_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Spec_frames\\Processed_frames\\Averaged_frames\\"

for file in os.listdir(spec_avrg_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"):
        wavelenght,shutter = get_avrg_info(filename)
        if shutter != '300.0':
            print('Adjusting frame: '+filename)
            frame = fits.getdata(spec_avrg_path + filename)
            frame = np.uint16(frame*(int(float(shutter))/300))
            hdu = fits.PrimaryHDU(frame)
            hdu.writeto(spec_avrg_path + "avrg_" + '300.0' + '_' + wavelenght + ".fit", overwrite=True)

"""
# Next, find the median values for each frame and format them into a text file
  
path = "E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\"
spec_avrg_path="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Spectral_sensitivity\\Spec_frames\\Processed_frames\\Averaged_frames\\"
f = open(path+'medianData.txt','w')

f.write("Wavelenght\tMedian\n")

for file in os.listdir(spec_avrg_path):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"): 
        frame = fits.getdata(spec_avrg_path + filename)
        frame = frame[193:293,274:374]
        #print(filename)
        wavelenght,shutter = get_avrg_info(filename)
        median = str(np.median(frame))
        #print(wavelenght,median)
        f.write(wavelenght+'\t'+median+'\n')

f.close()

# Next, divide the medians by the light source intensities by wavelenght to get the relative QE graph

