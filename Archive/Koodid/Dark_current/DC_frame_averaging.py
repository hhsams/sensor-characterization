import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os

DC_path = "E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Dark current\\Uus dark\\"

nr_of_frames = 1
frameShape = (1944, 2592)


def get_DCframe_info(filename):
    filename = filename.split('_')
    temp = filename[1].replace('C','')
    gain = filename[3]
    shutter = filename[4].replace('.fit','')
    
    return temp, gain, shutter

def add_frame(base,path,filename):
    print(filename)
    image_data = fits.getdata(path+filename)
    base = np.add(base,image_data)
    return base
  
#Read Dark cyrrebt files and find the means for each gains, exposures and temperatures
directory = os.fsencode(DC_path)
base = np.zeros(frameShape)
n = nr_of_frames

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".fit"): 
        if n == 0:
            base = add_frame(base,DC_path,filename)
            temperature,gain,shutter = get_DCframe_info(filename)
            print("....Averaging.... " + "Temperature: " + temperature + " C" + ", Gain: " + gain + ", Exposure: " + shutter)
            base = np.round(np.divide(base,nr_of_frames))
            np.savetxt(DC_path+"mean_DC_"+temperature+"_"+gain+"_"+shutter+".txt",base, delimiter='\t')
            base = np.zeros(frameShape)
            n = nr_of_frames+1
        else:
            base = add_frame(base,DC_path,filename)
        n -= 1
         #plt.imshow(image_data, cmap='gray')
         #plt.colorbar()
        #plt.imsave(path2+filename+".png",image_data,cmap='gray',vmin=10,vmax=700)
        # plt.show() .
        
        