import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import functions as func

LIN_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\"
LIN_DC_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Linearity DC\\"
LIN_processed_path ="E:\\Important stuff\\Projects\\Sensori testid\\ESEO sekundaar\\Linearity\\Processed\\"
directories = [LIN_path,LIN_DC_path,LIN_processed_path]


for file in os.listdir(directories[0]):
    LINfilename = os.fsdecode(file)
    if LINfilename.endswith(".txt"): 
        gain,shutter = func.get_LINframe_info(LINfilename,'.txt')
        DCfilename = func.find_DC_frame(directories[1],gain,shutter)
        if DCfilename != None:
            processed_frame = func.substract_frame(LINfilename,DCfilename,LIN_path,LIN_DC_path)
            np.savetxt(directories[2]+"processedLIN_"+gain+"_"+shutter+".txt",processed_frame, delimiter='\t')
        else:
            print("DC frame for: "+ LINfilename+ " not found!")

            